# GXFS WFE Documentation

## Table of Contents

1. [Overview](#overview)
2. [Core Features](#core-features)
3. [System Architecture](#system-architecture)
4. [Installation Guide](#installation-guide)
5. [Workflow Design & Implementation / Backend Integration](#workflow-design--implementation--backend-integration)
6. [User Interface Overview](#user-interface-overview)
7. [Sample Scenarios](#sample-scenarios)
8. [Conclusion and Future Directions](#conclusion)


# GXFS Scenario Engine Provisioning Tool Documentation

# Overview

The GXFS Authentication tool is a pivotal component of the Gaia-X Framework Services, designed to facilitate secure and efficient user authentication. It serves as the gateway for users to register and gain access to various services within the Gaia-X ecosystem. With an emphasis on security and user-friendliness, the authentication tool ensures that sensitive user data is handled with the utmost integrity, while providing a streamlined process for user onboarding.

At its core, the tool integrates a robust authentication mechanism that supports a wide range of services under the Gaia-X umbrella. Users can sign up using their email addresses and create a password, which then becomes their credentials for accessing the suite of tools and services offered by Gaia-X. Post-registration, the tool manages user sessions and securely handles login operations, providing a seamless interface between the user and Gaia-X services.

This document provides a comprehensive guide to the GXFS Authentication tool, covering its features, architecture, installation, and integration into backend systems. It is intended for system administrators, developers, and IT professionals who aim to implement or utilize the Gaia-X services.


# Core Features
The GXFS Authentication tool is engineered with a suite of features aimed at providing a secure, reliable, and user-friendly experience. Below are the core features that define the tool:

- **User Registration**: New users can easily register by providing essential details such as full name, email address, and password. The registration process is designed to be intuitive, guiding the user through each step with clear instructions and feedback.

- **Secure Login**: The tool offers a secure login mechanism that requires users to enter their registered email address and password. This process is protected by industry-standard security measures to prevent unauthorized access.

- **Service Selection**: Post-login, users have the flexibility to select from a range of available Gaia-X services. The tool's design accommodates a dropdown menu for service selection, which can be updated as new services are added to the ecosystem.

- **Password Encryption**: To ensure the security of user credentials, the GXFS Authentication tool employs robust encryption techniques. Passwords are hashed and stored securely, ensuring they cannot be read in plain text.

- **Responsive Design**: Responsive Design
The tool is built with a responsive design, ensuring compatibility with various devices and screen sizes, enhancing accessibility and user engagement.




# System Architecture


The GXFS Authentication tool integrates a front-end built with a UI builder and a Node-RED backend workflow engine, interfacing with a MongoDB database. This section describes the technical architecture and flow for both registration and login processes.

### Registration Flow

- **Front-end**: The registration interface is developed using a UI builder node, which is designed to collect essential user information such as full name, email address, and password via a web form.

- **Data Transmission**: When the user submits the registration form, the input data is sent to the backend via HTTP POST request.

- **Backend Workflow Engine (Node-RED)**: The Node-RED backend receives the data and executes a predefined flow. The flow includes nodes to handle various tasks:
    - **Email Check Node**: Queries the MongoDB to check if the email already exists using a `find` operation.
    - **Password Hashing Node**: Encrypts the password using a secure hashing algorithm before storage.
    - **User Creation Node**: Inserts a new user document into the MongoDB if the email is unique, using an `insert` operation.

- **MongoDB Database**: Serves as the data layer, storing user credentials and profiles. The database is accessed through a secure connection string, and operations are performed using MongoDB's native query language.

### Login Flow

- **Login UI**: Similar to registration, the login interface is constructed using the UI builder node, prompting for email, password, and service selection.

- **Authentication**: The backend workflow engine receives the login credentials and service choice, then performs the following operations:
    - **Credential Verification Node**: Checks the database for the user document matching the email and compares the hashed password.
    - **Service Availability Node**: Determines if the selected service is currently available to the user.
    - **Session Management Node**: Initiates a user session upon successful authentication, generating a session token.

- **Service Selection**: Post-authentication, the user is presented with a dropdown menu to select a service, such as GXUI or the Labeling Wizard. The selection is facilitated through a dynamic query to the database, retrieving available services for the user.

- **MongoDB**: Plays a pivotal role in authentication, storing and indexing user credentials for quick retrieval and validation. Passwords are stored using bcrypt or a similar hashing function to ensure security.

### Database Schema

- **Users Collection**: Stores user documents with fields for `name`, `email`, `hashedPassword`, and `services`. Indexed on `email` for efficient lookups.

### Security Considerations

- All communication with the backend is secured using HTTPS.
- Passwords are never stored in plaintext; only hashed versions are stored.
- The Node-RED backend is configured to prevent unauthorized flow modifications.
- MongoDB access is secured with role-based access control and encryption at rest.

### Modular Design

- The architecture is modular, allowing for new services and nodes to be added to the workflow without significant reconfiguration.
- The UI builder node can be updated to reflect changes in services or user interface requirements.

### Deployment

- The GXFS Authentication tool is containerized for ease of deployment. Dockerfiles and docker-compose.yml are provided for local development and production environments.
- Environment variables are used to configure database connections, service URLs, and other sensitive data.

This architecture provides a secure, scalable, and user-friendly authentication system for the Gaia-X Framework Services, ready for deployment and integration into the larger ecosystem.


# Installation Guide for GXFS Scenario Engine Provisioning Tool

## Step One: Install the Gaia-X Workflow Engine (WFE)
- Access the Gaia-X Workflow Engine repository through the following link: [Gaia-X WFE Repository](#link-to-wfe-repository).
- Follow the detailed instructions within the repository to install and initialize the WFE on your system.

## Step Two: Install node-red-contrib-uibuilder
1. Open your command-line interface.
2. Execute the following command to globally install the UI Builder node:
    ```bash
    npm install -g node-red-contrib-uibuilder

3. Proceed to install the required UI libraries into the UI Builder node with:
    ```bash
    npm install bootstrap bootstrap-vue http-vue-loader vue vue-router

## Step Three: 

## Step Four: 

## Step Five:

# Workflow Design & Implementation / Backend Integration

The Node-RED based backend orchestrates the authentication workflow, managing user registration and login with a clear, visual representation of the process flow. Here's an in-depth look at the implementation details for both processes.

### Workflow Design

A switch node at the beginning of the flow determines whether the incoming request is for registration or login, branching accordingly.

#### Registration Process

- **Input Capture**: The flow captures user inputs like email and password during the registration attempt.
- **Database Check**: A query to MongoDB checks if the email already exists.
- **User Notification**: If an existing email is detected, a modal popup informs the user of the duplicate account.
- **Account Creation**: New accounts have their password hashed and are then added to the database.
- **Confirmation**: A success message confirms account creation.
![Authentication RegisterBE](Auth/images/RegisterBE.png)


#### Login Process

- **Credential Capture**: User credentials are captured from the login form submission.
- **Existence Validation**: The database is queried to confirm the existence of the user.
- **Non-existence Notification**: A modal popup alerts the user if the account does not exist.
- **Password Verification**: Entered passwords are compared against the hashed passwords stored in the database.
- **Login Validation**: Incorrect credentials result in a user notification, while correct credentials lead to service redirection.
![Authentication LoginBE](Auth/images/LoginBE.png)

### Backend Integration

- **Node-RED**: Chosen for visual programming advantages, making complex workflows understandable and manageable.
- **MongoDB Operations**: Handled via Node-RED's MongoDB nodes, ensuring secure and efficient data management.
- **Security Measures**: Password hashing is implemented using bcrypt for robust security.
- **User Feedback**: Interactive modal popups for user notifications are created with Node-RED dashboard nodes.
- **Service Redirection**: Successful logins trigger redirection to the chosen Gaia-X service, handled within the flow.
- **Error Handling**: Dedicated nodes within the flow manage errors and user communication effectively.

The design of this workflow facilitates a secure and user-centric approach to authentication, allowing for straightforward backend integration and expansion with additional Gaia-X services in the future.



# User Interface Overview

## Registration Process

To register for the GXFS Toolbox, users are required to complete a few essential steps:

- **Full Name**: Enter your full name for personal identification within the system.
- **Email Address**: Provide a valid email address that will serve as your username.
- **Password & Confirmation**: Select a password, enter it, and then type it again in the confirmation field to ensure accuracy.

Once the form is completed, click on the "Sign up" button to create your account.
![Authentication Register User](Auth/images/RegisterUser.png)

## Login Process

For returning users, the login process is similarly streamlined:

- **Email Address**: Enter the email address associated with your GXFS Toolbox account.
- **Password**: Type in your password.
- **Service Selection**: From the dropdown menu, select the service you wish to access. Options include services like GXUI, among others.

After inputting your credentials and selecting the desired service, click the "Signin" button to access your account.
![Authentication Login User](Auth/images/LoginUser.png)


## Combined Overview

Both registration and login interfaces are designed with user-friendliness in mind, ensuring a seamless experience whether you are signing up for the first time or returning to the service. With a minimalist design, clear instructions, and straightforward navigation, users can quickly and securely access the GXFS Toolbox services. The interfaces also include quick links for additional support and account recovery options to assist users at every step.


# Sample Scenarios

A typical use case for the GXFS Toolbox involves a new user visiting the platform, completing the registration form by providing their full name, email, and password, and then signing up for an account. Upon successful registration, the user returns to the site, logs in with their credentials, selects a desired service from the dropdown—such as the GXUI or Labeling Wizard—and begins their session. This scenario exemplifies the ease of access and simplicity of the user journey from account creation to accessing various services within the GXFS ecosystem.

# Conclusion

In summary, the GXFS Toolbox presents a streamlined, secure, and user-centric approach to authentication and service access within the Gaia-X Framework Services. Throughout this documentation, we have explored the system's architecture, workflow design, backend integration, and the user interface for both registration and login processes. By implementing a straightforward user journey, the toolbox successfully lowers barriers to entry, ensuring that users can quickly leverage the powerful suite of services offered by Gaia-X. As the platform evolves, we anticipate continuous enhancements to further refine user experience and expand service offerings, solidifying the GXFS Toolbox's role as a cornerstone of the Gaia-X ecosystem.



